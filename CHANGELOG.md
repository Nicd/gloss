# Changelog

## 2.0.0

- Added Gettext support so that the created blog can be translated.
- Added header `lang` that can set the language of an individual page or post.
- Added language information to the Atom feed.
- Split `tags` and `archives` components out of the base layout for easier reuse.
- Made base layout `pre_render` function public so base layout is easier to customise.

## 1.0.2

Adjusted the title sizing.

## 1.0.1

Fix dependency to `lustre_ssg`.

## 1.0.0

Initial release.
