import { DateTime } from "./priv/vendor/luxon/src/luxon.js";
import { Ok, Error } from "./gleam.mjs";
import { Date, parse_month } from "./scriptorium/utils/date.mjs";
import { Time } from "./scriptorium/utils/time.mjs";

export function dateTimeInZone(dtStr, tz) {
  const dt = DateTime.fromISO(dtStr, { zone: tz });
  if (!dt.isValid) {
    return new Error(undefined);
  }

  return new Ok(dt);
}

export function utcNow() {
  return DateTime.utc();
}

export function toDate(dt) {
  return new Date(
    dt.year,
    // assert Ok
    parse_month(dt.month)[0],
    dt.day
  );
}

export function toTime(dt) {
  return new Time(dt.hour, dt.minute);
}

export function toRFC2822(dt) {
  return dt.toRFC2822();
}

export function toISO(dt) {
  return dt.toISO();
}
