/// A menu item that points to some URL (relative or absolute) and has a name.
pub type MenuItem {
  MenuItem(url: String, name: String)
}
