//// A day is a ranged integer from 1 to 31.

import bigi.{type BigInt}
import ranged_int/interface.{type Interface, Interface}

const max_limit = 31

const min_limit = 1

const iface: Interface(Day, interface.Overflowable) = Interface(
  from_bigint_unsafe: from_bigint_unsafe,
  to_bigint: to_bigint,
  limits: limits,
)

pub opaque type Day {
  Day(data: BigInt)
}

fn to_bigint(value: Day) {
  value.data
}

pub fn to_int(value: Day) {
  let assert Ok(int) = bigi.to_int(to_bigint(value))
  int
}

pub fn from_int(value: Int) {
  interface.from_bigint(bigi.from_int(value), iface)
}

pub fn compare(a: Day, b: Day) {
  interface.compare(a, b, iface)
}

fn limits() {
  interface.overflowable_limits(
    bigi.from_int(min_limit),
    bigi.from_int(max_limit),
  )
}

fn from_bigint_unsafe(value: BigInt) {
  Day(data: value)
}
