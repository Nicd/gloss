//// An ordered unbalanced tree.
////
//// Ordering of items is maintained as new items are inserted into the tree.
//// Sort of a replacement for an ordered list. Worst case performance for
//// insertion is O(n).

import gleam/order.{type Order, Eq, Gt, Lt}

/// Item ordering for when walking through a tree.
pub type WalkOrder {
  Asc
  Desc
}

/// Comparator function to resolve the order of items. Must return `Lt` if the
/// first argument is before the second, `Gt` if the opposite, or `Eq` if they
/// are equal.
pub type Comparator(a) =
  fn(a, a) -> Order

pub opaque type OrderedTree(a) {
  OrderedTree(root: Node(a), comparator: Comparator(a))
}

type Node(a) {
  Empty
  Node(before: Node(a), after: Node(a), value: a)
}

/// Create a new, empty tree, with the given comparator.
pub fn new(comparator: Comparator(a)) -> OrderedTree(a) {
  OrderedTree(root: Empty, comparator: comparator)
}

/// Insert a new item into the tree.
pub fn insert(tree: OrderedTree(a), item: a) -> OrderedTree(a) {
  OrderedTree(..tree, root: do_insert(tree.root, item, tree.comparator))
}

/// Fold over the elements in the tree in the given order.
pub fn fold(
  over tree: OrderedTree(a),
  from initial: b,
  order order: WalkOrder,
  with fun: fn(b, a) -> b,
) -> b {
  do_fold(tree.root, initial, fun, order)
}

fn do_fold(node: Node(a), acc: b, fun: fn(b, a) -> b, order: WalkOrder) -> b {
  case node {
    Empty -> acc
    Node(before: before, after: after, value: value) -> {
      case order {
        Desc -> {
          let afters = do_fold(after, acc, fun, order)
          do_fold(before, fun(afters, value), fun, order)
        }
        Asc -> {
          let befores = do_fold(before, acc, fun, order)
          do_fold(after, fun(befores, value), fun, order)
        }
      }
    }
  }
}

/// Get the amount of items in the tree.
///
/// This operation runs in O(n) time.
pub fn length(tree: OrderedTree(a)) -> Int {
  fold(tree, 0, Asc, fn(acc, _item) { acc + 1 })
}

fn do_insert(node: Node(a), item: a, comparator: Comparator(a)) -> Node(a) {
  case node {
    Empty -> new_node(item)
    Node(before: before, after: after, value: value) -> {
      case comparator(value, item) {
        Lt | Eq ->
          Node(
            before: before,
            after: do_insert(after, item, comparator),
            value: value,
          )
        Gt ->
          Node(
            before: do_insert(before, item, comparator),
            after: after,
            value: value,
          )
      }
    }
  }
}

fn new_node(item: a) -> Node(a) {
  Node(before: Empty, after: Empty, value: item)
}

/// Get the tree as a list in the given list order.
pub fn to_list(tree: OrderedTree(a), order: WalkOrder) -> List(a) {
  fold(
    tree,
    [],
    case order {
      Asc -> Desc
      Desc -> Asc
    },
    fn(acc, item) { [item, ..acc] },
  )
}
