//// Bindings to the Luxon library.

import scriptorium/utils/date.{type Date}
import scriptorium/utils/time.{type Time}

/// Luxon DateTime.
pub type DateTime

/// Get a Luxon DateTime in the given timezone.
pub fn date_time_in_zone(date: Date, time: Time, tz: String) {
  let datetime_str = date.format_iso(date) <> "T" <> time.format_iso(time)
  do_date_time_in_zone(datetime_str, tz)
}

/// Get the current time as UTC.
@external(javascript, "../../ffi_luxon.mjs", "utcNow")
pub fn utc_now() -> DateTime

@external(javascript, "../../ffi_luxon.mjs", "dateTimeInZone")
fn do_date_time_in_zone(
  datetime_str: String,
  tz: String,
) -> Result(DateTime, Nil)

/// Format the DateTime as an ISO 8601 format string.
@external(javascript, "../../ffi_luxon.mjs", "toISO")
pub fn to_iso(dt: DateTime) -> String
