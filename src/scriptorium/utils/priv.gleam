import gleam/uri
import scriptorium/internal/utils/meta_url
import scriptorium/internal/utils/path

/// Get the path to the `priv` directory of `scriptorium`.
pub fn path() -> String {
  let assert Ok(meta_url) = uri.parse(meta_url.get())

  path.dirname(meta_url.path) <> "/priv"
}
