import bigi.{type BigInt}

/// A unique ID.
pub type UniqID =
  BigInt

/// Unique ID generator.
pub opaque type Generator {
  Generator(id: BigInt)
}

/// Get a new generator.
pub fn new() -> Generator {
  Generator(bigi.zero())
}

/// Get a unique ID and a new generator, that can be used to generate a new ID.
pub fn get(gen: Generator) -> #(UniqID, Generator) {
  let new = bigi.add(gen.id, bigi.from_int(1))
  #(new, Generator(new))
}
