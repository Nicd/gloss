import gleam/bool
import gleam/int
import gleam/order.{type Order, Eq}
import gleam/string
import kielet.{gettext as g_}
import kielet/context.{type Context}
import scriptorium/utils/ints/day.{type Day}

pub type Month {
  Jan
  Feb
  Mar
  Apr
  May
  Jun
  Jul
  Aug
  Sep
  Oct
  Nov
  Dec
}

/// All months in order.
pub const months = [Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec]

/// A date with 1-indexed years and days.
pub type Date {
  Date(year: Int, month: Month, day: Day)
}

/// Parse an integer into a month.
pub fn parse_month(month_int: Int) -> Result(Month, Nil) {
  case month_int {
    1 -> Ok(Jan)
    2 -> Ok(Feb)
    3 -> Ok(Mar)
    4 -> Ok(Apr)
    5 -> Ok(May)
    6 -> Ok(Jun)
    7 -> Ok(Jul)
    8 -> Ok(Aug)
    9 -> Ok(Sep)
    10 -> Ok(Oct)
    11 -> Ok(Nov)
    12 -> Ok(Dec)
    _other -> Error(Nil)
  }
}

/// Get the number of days in a month in a given year.
pub fn days_in_month(month: Month, year: Int) {
  case month {
    Jan -> 31
    Feb -> {
      case year % 4 {
        0 -> {
          case year % 100 {
            0 -> {
              case year % 400 {
                0 -> 29
                _ -> 28
              }
            }
            _ -> 29
          }
        }
        _ -> 28
      }
    }
    Mar -> 31
    Apr -> 30
    May -> 31
    Jun -> 30
    Jul -> 31
    Aug -> 31
    Sep -> 30
    Oct -> 31
    Nov -> 30
    Dec -> 31
  }
}

/// Check if a given date is valid.
pub fn is_valid_date(date: Date) -> Bool {
  let day = day.to_int(date.day)
  use <- bool.guard(day < 1, False)
  use <- bool.guard(day <= days_in_month(date.month, date.year), False)
  True
}

/// Compare if `a` is before (lower than) `b`.
pub fn compare(a: Date, b: Date) -> Order {
  case int.compare(a.year, b.year) {
    order.Eq -> {
      case int.compare(month_to_int(a.month), month_to_int(b.month)) {
        order.Eq -> day.compare(a.day, b.day)
        other -> other
      }
    }
    other -> other
  }
}

/// Convert a month to a 1-indexed int.
pub fn month_to_int(month: Month) -> Int {
  case month {
    Jan -> 1
    Feb -> 2
    Mar -> 3
    Apr -> 4
    May -> 5
    Jun -> 6
    Jul -> 7
    Aug -> 8
    Sep -> 9
    Oct -> 10
    Nov -> 11
    Dec -> 12
  }
}

/// Convert a month to a month name string.
pub fn month_to_string(month: Month, context: Context) -> String {
  case month {
    Jan -> g_(context, "January")
    Feb -> g_(context, "February")
    Mar -> g_(context, "March")
    Apr -> g_(context, "April")
    May -> g_(context, "May")
    Jun -> g_(context, "June")
    Jul -> g_(context, "July")
    Aug -> g_(context, "August")
    Sep -> g_(context, "September")
    Oct -> g_(context, "October")
    Nov -> g_(context, "November")
    Dec -> g_(context, "December")
  }
}

/// Convert a month to a short month name string.
pub fn month_to_short_string(month: Month, context: Context) -> String {
  case month {
    Jan -> g_(context, "Jan")
    Feb -> g_(context, "Feb")
    Mar -> g_(context, "Mar")
    Apr -> g_(context, "Apr")
    May -> g_(context, "May")
    Jun -> g_(context, "Jun")
    Jul -> g_(context, "Jul")
    Aug -> g_(context, "Aug")
    Sep -> g_(context, "Sep")
    Oct -> g_(context, "Oct")
    Nov -> g_(context, "Nov")
    Dec -> g_(context, "Dec")
  }
}

/// Format a date as a string.
pub fn format(date: Date, context: Context) -> String {
  g_(context, "{day} {month_short_str} {year}")
  |> string.replace("{day}", int.to_string(day.to_int(date.day)))
  |> string.replace(
    "{day_0}",
    string.pad_left(int.to_string(day.to_int(date.day)), 2, "0"),
  )
  |> string.replace("{month_str}", month_to_string(date.month, context))
  |> string.replace(
    "{month_short_str}",
    month_to_short_string(date.month, context),
  )
  |> string.replace("{month_no}", int.to_string(month_to_int(date.month)))
  |> string.replace(
    "{month_no0}",
    string.pad_left(int.to_string(month_to_int(date.month)), 2, "0"),
  )
  |> string.replace("{year}", int.to_string(date.year))
}

/// Format a date in the ISO 8601 format.
pub fn format_iso(date: Date) -> String {
  int.to_string(date.year)
  <> "-"
  <> string.pad_left(int.to_string(month_to_int(date.month)), 2, "0")
  <> "-"
  <> string.pad_left(int.to_string(day.to_int(date.day)), 2, "0")
}
