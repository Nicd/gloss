import gleam/list
import gleam/result
import gleam/string
import scriptorium/models/page.{type Page, Page}
import scriptorium/parser/common.{type ParseError, EmptyFile, HeaderMissing, try}

/// Parse page from file data.
pub fn parse(filename: String, contents: String) -> Result(Page, ParseError) {
  let lines = string.split(contents, "\n")

  use title <- try(list.first(lines), EmptyFile)
  use rest <- try(list.rest(lines), HeaderMissing)
  let slug =
    string.slice(
      filename,
      0,
      string.length(filename) - string.length(common.filename_postfix),
    )

  let #(headers, body) =
    list.split_while(rest, fn(line) { !string.is_empty(line) })

  use headers <- result.try(common.parse_headers(headers))
  let body = string.join(body, "\n")

  Ok(Page(title: title, slug: slug, headers: headers, content: body))
}
