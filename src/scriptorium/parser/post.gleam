import gleam/bool
import gleam/int
import gleam/list
import gleam/option
import gleam/regex
import gleam/result
import gleam/string
import scriptorium/models/header.{type Header}
import scriptorium/models/post.{type Post, type PostedAt, type Tag, Post}
import scriptorium/parser/common.{
  type ParseError, DayNotInt, EmptyFile, HeaderMissing, InvalidDate,
  MalformedFilename, MalformedHeader, MonthNotInt, YearNotInt, try,
}
import scriptorium/utils/date.{Date}
import scriptorium/utils/ints/day
import scriptorium/utils/luxon
import scriptorium/utils/time.{type Time, Time}

/// Post filenames must match this regex.
pub const filename_regex = "^\\d{4}-\\d\\d-\\d\\d-.*\\.md$"

const filename_separator = "-"

const tag_separator = ","

const split_re = "<!--\\s*SPLIT\\s*-->"

type FilenameMeta {
  FilenameMeta(date: PostedAt, order: Int, slug: String)
}

/// Parse post from file data.
pub fn parse(filename: String, contents: String) -> Result(Post, ParseError) {
  let filename =
    string.slice(
      filename,
      0,
      string.length(filename) - string.length(common.filename_postfix),
    )
  use meta <- result.try(parse_filename_meta(filename))

  let lines = string.split(contents, "\n")

  use title <- try(list.first(lines), EmptyFile)
  use rest <- try(list.rest(lines), HeaderMissing)

  let #(headers, body) =
    list.split_while(rest, fn(line) { !string.is_empty(line) })

  let #(tags, headers) = case headers {
    [] -> #("", [])
    [tags, ..headers] ->
      case common.parse_header(tags) {
        Ok(_) -> #("", [tags, ..headers])
        Error(_) -> #(tags, headers)
      }
  }

  let tags = parse_tags(tags)
  use headers <- result.try(common.parse_headers(headers))
  let body = string.join(body, "\n")
  let short_content = parse_short_content(body)
  use time <- result.try(parse_time(headers))

  let assert post.JustDate(just_date) = meta.date
  use date <- result.try(case time {
    option.Some(#(time, tz)) -> {
      use dt <- result.try(
        luxon.date_time_in_zone(just_date, time, tz)
        |> result.replace_error(InvalidDate),
      )
      Ok(post.DateTime(just_date, time, tz, dt))
    }
    option.None -> Ok(meta.date)
  })

  Ok(Post(
    title: title,
    slug: meta.slug,
    date: date,
    content: body,
    headers: headers,
    order: meta.order,
    short_content: short_content,
    tags: tags,
  ))
}

fn parse_filename_meta(filename: String) -> Result(FilenameMeta, ParseError) {
  let filename_parts = string.split(filename, filename_separator)
  let #(meta_parts, rest_parts) = list.split(filename_parts, 4)

  use #(year_str, month_str, day_str, maybe_order) <- result.try(case
    meta_parts
  {
    [y, m, d, o] -> Ok(#(y, m, d, o))
    _ -> Error(MalformedFilename)
  })

  use year <- try(int.parse(year_str), YearNotInt)
  use month_int <- try(int.parse(month_str), MonthNotInt)
  use month <- try(date.parse_month(month_int), InvalidDate)
  use day <- try(int.parse(day_str), DayNotInt)
  use day <- try(day.from_int(day), InvalidDate)
  let #(order, slug) = parse_order_slug(maybe_order, rest_parts)

  let date = Date(year: year, month: month, day: day)
  use <- bool.guard(date.is_valid_date(date), Error(InvalidDate))

  Ok(FilenameMeta(
    date: post.JustDate(date),
    order: option.unwrap(order, 0),
    slug: slug,
  ))
}

fn parse_order_slug(
  maybe_order: String,
  rest_parts: List(String),
) -> #(option.Option(Int), String) {
  let fail_case = fn() {
    #(option.None, string.join([maybe_order, ..rest_parts], filename_separator))
  }

  case string.length(maybe_order) {
    o if o >= 1 && o <= 2 -> {
      case int.parse(maybe_order) {
        Ok(order) -> #(
          option.Some(order),
          string.join(rest_parts, filename_separator),
        )
        _ -> fail_case()
      }
    }
    _ -> fail_case()
  }
}

fn parse_tags(tags: String) -> List(Tag) {
  let tags =
    tags
    |> string.split(tag_separator)
    |> list.map(string.trim)

  case tags {
    [""] -> []
    other -> other
  }
}

fn parse_short_content(body: String) -> option.Option(String) {
  let assert Ok(re) =
    regex.compile(
      split_re,
      regex.Options(case_insensitive: False, multi_line: False),
    )
  let body_parts = regex.split(re, body)

  case body_parts {
    [] | [_] -> option.None
    [first, ..] -> option.Some(first)
  }
}

fn parse_time(
  headers: List(Header),
) -> Result(option.Option(#(Time, String)), ParseError) {
  let time_hdr = list.key_find(headers, "time")
  case time_hdr {
    Error(Nil) -> Ok(option.None)
    Ok(hdr) ->
      case list.split(string.split(hdr, " "), 1) {
        #([ts], rest) ->
          time.parse(ts)
          |> result.replace_error(InvalidDate)
          |> result.map(fn(ts) { option.Some(#(ts, string.join(rest, " "))) })
        _ -> Error(MalformedHeader("time: " <> hdr))
      }
  }
}
