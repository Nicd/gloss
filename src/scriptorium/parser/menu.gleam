import gleam/list
import gleam/result
import gleam/string
import scriptorium/models/menu.{MenuItem}

/// Parse the content and return menu items.
pub fn parse(content: String) {
  string.split(content, "\n")
  |> list.filter(fn(line) { string.length(line) != 0 })
  |> list.map(fn(line) {
    use #(url, name) <- result.try(string.split_once(line, " "))
    Ok(MenuItem(url: url, name: name))
  })
  |> result.all()
}
