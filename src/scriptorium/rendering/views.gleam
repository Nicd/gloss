import lustre/element.{type Element}
import scriptorium/compiler.{type CompiledPage, type CompiledPost}
import scriptorium/models/page
import scriptorium/models/post

/// The base view renders the base page layout.
///
/// The three arguments are:
/// - The inner content to render in the layout.
/// - Extra elements to put inside `<head>`.
/// - Text to add as a prefix to the `<title>` element.
pub type BaseView =
  fn(Element(Nil), List(Element(Nil)), String) -> Element(Nil)

/// Types of pages in the blog.
pub type PageType {
  /// Individual post.
  Post(post.Post)
  /// Individual page.
  Page(page.Page)
  /// Any other page.
  Other(title: String, description: String)
}

/// The meta view renders meta tags such as OpenGraph based on the page information.
pub type MetaView =
  fn(PageType) -> List(Element(Nil))

/// View to render an individual post.
pub type SinglePostView =
  fn(CompiledPost) -> Element(Nil)

/// View to render an individual page.
pub type PageView =
  fn(CompiledPage) -> Element(Nil)

/// View to render the RSS or Atom feed.
pub type FeedView =
  fn(List(CompiledPost)) -> Element(Nil)

/// Information passed for list pages.
pub type ListInfo {
  ListInfo(
    /// The path to prefix before the page number. E.g. `/index` and the resulting path would then be `/index/2`.
    root_path: String,
    /// The page number of the current page being rendered.
    current_page: Int,
    /// The amount of pages in the current list.
    total_pages: Int,
    /// Posts on this page.
    posts: List(CompiledPost),
    /// Any extra content to put on top of the page before the posts.
    extra_header: Element(Nil),
  )
}

/// View to render a list page. A list page is a page with many posts in a list.
pub type ListPageView =
  fn(ListInfo) -> Element(Nil)
