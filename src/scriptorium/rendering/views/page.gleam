import gleam/list
import lustre/attribute.{attribute, class}
import lustre/element.{text}
import lustre/element/html.{article, div, h2, header}
import scriptorium/compiler.{type CompiledPage}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}

pub fn generate(db: Database, config: Configuration) {
  view(_, db, config)
}

fn view(page: CompiledPage, _db: Database, _config: Configuration) {
  let language = case list.key_find(page.orig.headers, "lang") {
    Ok(l) -> [attribute("lang", l)]
    Error(_) -> []
  }

  article([class("page"), ..language], [
    header([], [h2([], [text(page.orig.title)])]),
    div([attribute("dangerous-unescaped-html", page.content)], []),
  ])
}
