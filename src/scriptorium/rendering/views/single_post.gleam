import gleam/list
import gleam/option
import gleam/string
import kielet.{gettext as g_}
import kielet/context.{type Context}
import lustre/attribute.{attribute, class, href}
import lustre/element.{type Element, text}
import lustre/element/html.{
  a, article, div, footer, h2, header, li, nav, p, time, ul,
}
import scriptorium/compiler.{type CompiledPost}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/models/post.{type Post}
import scriptorium/utils/date
import scriptorium/utils/luxon
import scriptorium/utils/time

/// Generate a view that renders a full post.
pub fn full_view(db: Database, config: Configuration) {
  view(_, True, db, config)
}

/// Generate a view that renders a post that's inside of a list.
pub fn list_view(db: Database, config: Configuration) {
  view(_, False, db, config)
}

fn view(post: CompiledPost, is_full: Bool, _db: Database, config: Configuration) {
  let post_url =
    config.paths.html(config.paths.root <> config.paths.single_post(post.orig))

  let content = case post.content.short, is_full {
    option.Some(content), False -> content
    _, _ -> post.content.full
  }

  let language = case list.key_find(post.orig.headers, "lang") {
    Ok(l) -> [attribute("lang", l)]
    Error(_) -> []
  }

  article([class("post"), ..language], [
    header([], [
      wrap_heading(h2([], [text(post.orig.title)]), post_url, is_full),
      p(
        [class("post__time"), attribute("lang", config.l10n.language)],
        post_time(post.orig, config.l10n.context),
      ),
      nav(
        [
          attribute("lang", config.l10n.language),
          attribute("aria-label", g_(config.l10n.context, "Tags")),
        ],
        [
          ul(
            [],
            list.map(post.orig.tags, fn(tag) {
              li([], [
                a(
                  [
                    href(config.paths.html(
                      config.paths.root <> config.paths.tag(tag),
                    )),
                  ],
                  [text(tag)],
                ),
              ])
            }),
          ),
        ],
      ),
    ]),
    div([attribute("dangerous-unescaped-html", content)], []),
    case is_full, post.content.short {
      True, _ | False, option.None -> element.none()
      False, option.Some(_) ->
        footer([attribute("lang", config.l10n.language)], [
          a([href(post_url)], [text(g_(config.l10n.context, "Read more…"))]),
        ])
    },
  ])
}

fn wrap_heading(heading: Element(Nil), post_url: String, is_full: Bool) {
  case is_full {
    True -> heading
    False -> a([href(post_url)], [heading])
  }
}

fn post_time(post: Post, context: Context) {
  let post_date = post.get_date(post)

  let human_str = case post.get_time(post) {
    option.Some(t) ->
      g_(context, "{date}, {time}")
      |> string.replace("{date}", date.format(post_date, context))
      |> string.replace("{time}", time.format(t, context))
    option.None -> date.format(post_date, context)
  }

  let datetime_str = case post.get_luxon(post) {
    option.Some(lx) -> luxon.to_iso(lx)
    option.None -> date.format_iso(post_date)
  }

  let datetime_el =
    time([attribute("datetime", datetime_str)], [text(human_str)])

  let format = g_(context, "Posted on {datetime}.")
  string.split(format, "{datetime}")
  |> list.map(text)
  |> list.intersperse(datetime_el)
}
