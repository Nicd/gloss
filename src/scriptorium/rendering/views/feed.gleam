import gleam/list
import gleam/option
import lustre/attribute.{attribute}
import lustre/element
import lustre/ssg/atom.{
  author, content, email, entry, feed, generator, id, link, name, rights,
  summary, title, updated, uri,
}
import scriptorium/compiler.{type CompiledPost}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/models/post
import scriptorium/utils/luxon

pub fn generate(_db: Database, config: Configuration) {
  fn(posts: List(CompiledPost)) {
    let now = luxon.utc_now()
    let #(posts, _) = list.split(posts, config.rendering.posts_in_feed)

    feed([attribute("xml:lang", config.l10n.language)], [
      title([], config.blog_name),
      updated([], luxon.to_iso(now)),
      link([attribute("href", config.blog_url)]),
      id([], config.blog_url),
      author([], [
        name([], config.author.name),
        case config.author.email {
          option.Some(e) -> email([], e)
          option.None -> element.none()
        },
        case config.author.url {
          option.Some(u) -> uri([], u)
          option.None -> element.none()
        },
      ]),
      generator(
        [
          attribute("uri", "https://gitlab.com/Nicd/scriptorium"),
          attribute("version", "2.0.0"),
        ],
        "Scriptorium",
      ),
      rights([], config.rendering.copyright),
      ..list.map(posts, fn(post) {
        let url =
          config.blog_url
          <> config.paths.html(config.paths.single_post(post.orig))
        let date_str = post.to_iso8601(post.orig)
        let entry_attrs = case list.key_find(post.orig.headers, "lang") {
          Ok(l) -> [attribute("xml:lang", l)]
          Error(_) -> []
        }

        entry(entry_attrs, [
          title([], post.orig.title),
          id([], url),
          link([attribute("href", url)]),
          updated([], date_str),
          content([], post.content.full),
          case post.content.short {
            option.Some(s) -> summary([], s)
            option.None -> element.none()
          },
        ])
      })
    ])
  }
}
