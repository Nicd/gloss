import gleam/int
import gleam/list
import kielet.{gettext as g_}
import lustre/attribute.{attribute, href}
import lustre/element.{text}
import lustre/element/html.{a, li, span, ul}
import scriptorium/config.{type Configuration}
import scriptorium/rendering/views.{type ListInfo}

pub fn view(list_info: ListInfo, root_path: String, config: Configuration) {
  let page_range = list.range(1, list_info.total_pages)

  ul(
    [],
    list.flatten([
      [
        nextprev_link(
          list_info.current_page > 1,
          list_info.current_page - 1,
          g_(config.l10n.context, "Previous"),
          "«",
          root_path,
          config,
        ),
      ],
      list.map(page_range, fn(page) {
        let link_text = case page == list_info.current_page {
          True -> "[" <> int.to_string(page) <> "]"
          False -> int.to_string(page)
        }

        let link_attributes = case page == list_info.current_page {
          True -> [
            attribute(
              "aria-label",
              int.to_string(page)
                <> " ("
                <> g_(config.l10n.context, "current page")
                <> ")",
            ),
            attribute("aria-disabled", "true"),
          ]
          False -> []
        }

        li([], [
          a(
            list.flatten([
              [
                href(config.paths.html(
                  config.paths.root <> config.paths.list_page(root_path, page),
                )),
              ],
              link_attributes,
            ]),
            [text(link_text)],
          ),
        ])
      }),
      [
        nextprev_link(
          list_info.current_page < list_info.total_pages,
          list_info.current_page + 1,
          g_(config.l10n.context, "Next"),
          "»",
          root_path,
          config,
        ),
      ],
    ]),
  )
}

fn nextprev_link(
  condition: Bool,
  target: Int,
  label: String,
  text: String,
  root_path: String,
  config: Configuration,
) {
  case condition {
    True ->
      li([], [
        a(
          [
            href(config.paths.html(
              config.paths.root <> config.paths.list_page(root_path, target),
            )),
            attribute("aria-label", label),
          ],
          [span([attribute("aria-hidden", "true")], [element.text(text)])],
        ),
      ])
    False -> element.none()
  }
}
