//// The archives component renders all the years and months where there are
//// posts as nested unordered lists.

import gleam/dict
import gleam/int
import gleam/list
import lustre/attribute.{href}
import lustre/element.{text}
import lustre/element/html.{a, li, ul}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/utils/date
import scriptorium/utils/ordered_tree

pub fn view(db: Database, config: Configuration) {
  db
  |> database.years()
  |> dict.to_list()
  |> list.sort(fn(a, b) { int.compare(a.0, b.0) })
  |> list.fold([], fn(year_archive, year) {
    let #(year, months) = year
    [
      li([], [
        a(
          [
            href(config.paths.html(
              config.paths.root
              <> config.paths.list_page(config.paths.year(year), 1),
            )),
          ],
          [text(int.to_string(year))],
        ),
        ul(
          [],
          list.fold(date.months, [], fn(month_archive, month) {
            case dict.get(months, month) {
              Ok(posts) -> [
                li([], [
                  a(
                    [
                      href(config.paths.html(
                        config.paths.root
                        <> config.paths.list_page(
                          config.paths.month(year, month),
                          1,
                        ),
                      )),
                    ],
                    [
                      text(
                        date.month_to_string(month, config.l10n.context)
                        <> " ("
                        <> int.to_string(ordered_tree.length(posts))
                        <> ")",
                      ),
                    ],
                  ),
                ]),
                ..month_archive
              ]
              Error(_) -> month_archive
            }
          }),
        ),
      ]),
      ..year_archive
    ]
  })
  |> ul([], _)
}
