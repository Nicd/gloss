//// The tags component renders tags of the database as a tag cloud with tags of
//// varying sizes as an unordered list.

import gleam/dict
import gleam/float
import gleam/int
import gleam/list
import gleam/string
import lustre/attribute.{href, style}
import lustre/element.{text}
import lustre/element/html.{a, li, ul}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/utils/ordered_tree

const tag_min_size = 0.5

pub fn view(db: Database, config: Configuration) {
  let tags =
    db
    |> database.tags()
    |> dict.map_values(fn(_key, posts) {
      int.to_float(ordered_tree.length(posts))
    })

  let most_posts =
    tags
    |> dict.values()
    |> list.fold(0.0, float.max)

  tags
  |> dict.to_list()
  |> list.sort(fn(a, b) {
    string.compare(string.lowercase(a.0), string.lowercase(b.0))
  })
  |> list.map(fn(item) {
    let #(tag, post_count) = item
    let percentage =
      float.round(
        {
          { { post_count /. most_posts } *. { 1.0 -. tag_min_size } }
          +. tag_min_size
        }
        *. 100.0,
      )

    li([], [
      a(
        [
          href(config.paths.html(config.paths.root <> config.paths.tag(tag))),
          style([#("font-size", int.to_string(percentage) <> "%")]),
        ],
        [text(tag)],
      ),
    ])
  })
  |> ul([], _)
}
