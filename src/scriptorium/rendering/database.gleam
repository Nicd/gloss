//// The render database stores the rendered posts and pages.

import gleam/dict.{type Dict}
import scriptorium/models/page.{type Page}
import scriptorium/models/post.{type Post}
import scriptorium/utils/date.{type Month}
import lustre/element.{type Element}

/// A post and its rendered content.
pub type RenderedSinglePost {
  RenderedSinglePost(orig: Post, content: Element(Nil))
}

/// A page and its rendered content.
pub type RenderedPage {
  RenderedPage(page: Page, content: Element(Nil))
}

/// A list page's page number and the page's content.
pub type RenderedListPage {
  ListPage(page: Int, content: Element(Nil))
}

pub type RenderDatabase {
  RenderDatabase(
    /// Individual posts.
    single_posts: List(RenderedSinglePost),
    /// Individual pages.
    pages: List(RenderedPage),
    /// "Index" list pages, meaning the main post flow.
    index_pages: List(RenderedListPage),
    /// Tag list pages.
    tag_pages: Dict(String, List(RenderedListPage)),
    /// Year list pages.
    year_pages: Dict(Int, List(RenderedListPage)),
    /// Month list pages.
    month_pages: Dict(#(Int, Month), List(RenderedListPage)),
    /// The feed (corresponding to the main post flow).
    feed: Element(Nil),
  )
}
