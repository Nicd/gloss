import kielet/database
import kielet/language
import scriptorium/utils/priv
import simplifile

const locale_dir = "locale"

const locale_sub_dir = "LC_MESSAGES"

const locale_filename = "messages.mo"

/// Load builtin translations and return a translation database containing them.
pub fn load_builtins() -> database.Database {
  let db = database.new()
  let assert Ok(fi_data) =
    simplifile.read_bits(
      priv.path()
      <> "/"
      <> locale_dir
      <> "/"
      <> "fi"
      <> "/"
      <> locale_sub_dir
      <> "/"
      <> locale_filename,
    )
  let assert Ok(fi) = language.load("fi", fi_data)

  database.add_language(db, fi)
}
