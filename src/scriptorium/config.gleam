//// The configuration is the main way to customize Scriptorium's behaviour.

import gleam/option
import kielet/context.{type Context}
import scriptorium/compiler.{type CompileDatabase, type Compiler}
import scriptorium/models/database.{type Database}
import scriptorium/paths.{type PathConfiguration}
import scriptorium/rendering/database.{type RenderDatabase} as _
import scriptorium/rendering/views.{
  type BaseView, type FeedView, type ListPageView, type MetaView, type PageView,
  type SinglePostView,
}

/// An error occurred when writing the rendered results into files.
pub type WriteError {
  WriteError(err: String)
}

/// An error occurred when parsing input files.
pub type ParseError {
  ParseError(err: String)
}

/// The parser
pub type Parser =
  fn(Configuration) -> Result(Database, ParseError)

/// Renders the content of the database into HTML.
pub type Renderer =
  fn(Database, CompileDatabase, Configuration) -> RenderDatabase

/// Writes the rendered HTML into files.
pub type Writer =
  fn(RenderDatabase, Configuration) -> Result(Nil, WriteError)

/// View generators for the blog. These take the database and configuration and
/// must return a view function that is used for rendering.
///
/// See the `scriptorium/rendering/views` documentation for descriptions of the
/// individual views.
pub type Views {
  Views(
    base: fn(Database, Configuration) -> BaseView,
    meta: fn(Database, Configuration) -> MetaView,
    /// View for a single post that is rendered on its own.
    single_post_full: fn(Database, Configuration) -> SinglePostView,
    /// View for a single post rendered as part of a list.
    single_post_list: fn(Database, Configuration) -> SinglePostView,
    page: fn(Database, Configuration) -> PageView,
    list_page: fn(Database, Configuration) -> ListPageView,
    feed: fn(Database, Configuration) -> FeedView,
  )
}

/// Compiling related configuration.
pub type Compiling {
  Compiling(
    /// The compiler to use for compiling the database.
    database_compiler: fn(Database, Compiler) -> CompileDatabase,
    /// The compiler to use for compiling individual items.
    item_compiler: Compiler,
  )
}

/// Rendering related configuration.
pub type Rendering {
  Rendering(
    /// The renderer to use.
    renderer: Renderer,
    /// The view generators to use.
    views: Views,
    /// The copyright statement of the blog, used in the footer and the feed.
    copyright: String,
    /// How many posts to show per page.
    posts_per_page: Int,
    /// How many posts to render in the feed.
    posts_in_feed: Int,
  )
}

/// Author related configuration.
pub type Author {
  Author(
    /// Name of the author.
    name: String,
    /// Email address of the author, used in the feed's author information.
    email: option.Option(String),
    /// Website URL of the author, used in the feed's author information and
    /// added as a `rel="me"` link in the base layout.
    url: option.Option(String),
  )
}

/// Localization related configuration.
pub type Localization {
  Localization(
    /// Language code of the blog, meaning the language of the content you are
    /// writing. The language code must be according to
    /// <https://datatracker.ietf.org/doc/html/rfc5646>.
    language: String,
    /// Context to use for gettext operations for translating built-in strings.
    context: Context,
  )
}

pub type Configuration {
  Configuration(
    /// Name of the blog.
    blog_name: String,
    /// Absolute URL of the blog, this is where you are deploying it.
    blog_url: String,
    author: Author,
    l10n: Localization,
    compiling: Compiling,
    rendering: Rendering,
    paths: PathConfiguration,
    parser: Parser,
    writer: Writer,
    /// The folder to write the output into.
    output_path: String,
  )
}
