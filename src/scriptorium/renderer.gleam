//// The renderer's job is to render compiled content into HTML. By default this
//// means Lustre elements that can be later stringified.

import gleam/dict.{type Dict}
import gleam/int
import gleam/list
import gleam/result
import gleam/string
import kielet.{gettext as g_}
import lustre/element.{type Element}
import scriptorium/compiler.{
  type CompileDatabase, type CompiledPage, type CompiledPost,
}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database, type PostID, type PostWithID}
import scriptorium/rendering/database.{
  type RenderDatabase, type RenderedSinglePost, ListPage, RenderDatabase,
  RenderedPage, RenderedSinglePost,
} as _
import scriptorium/rendering/views.{
  type BaseView, type FeedView, type ListPageView, type MetaView, type PageView,
  type SinglePostView, ListInfo,
}
import scriptorium/utils/date
import scriptorium/utils/ordered_tree

/// Helper struct to pass all the used views to the rendering functions.
pub type Views {
  Views(
    base: BaseView,
    meta: MetaView,
    /// View for a single post rendered on its own.
    single_post_full: SinglePostView,
    page: PageView,
    list_page: ListPageView,
    feed: FeedView,
  )
}

/// Render the database and compiled content using the configuration.
pub fn render(
  db: Database,
  compiled: CompileDatabase,
  config: Configuration,
) -> RenderDatabase {
  let views =
    Views(
      base: config.rendering.views.base(db, config),
      meta: config.rendering.views.meta(db, config),
      single_post_full: config.rendering.views.single_post_full(db, config),
      page: config.rendering.views.page(db, config),
      list_page: config.rendering.views.list_page(db, config),
      feed: config.rendering.views.feed(db, config),
    )

  let all_posts = database.get_posts_with_ids(db, ordered_tree.Desc)
  let posts = render_posts(db, compiled.posts, views)
  let pages = render_pages(db, compiled.pages, views)
  let index_pages = render_index_pages(config, all_posts, compiled.posts, views)
  let tag_pages = render_tag_pages(config, db, compiled.posts, views)
  let year_pages = render_year_pages(config, db, compiled.posts, views)
  let month_pages = render_month_pages(config, db, compiled.posts, views)
  let feed = render_feed(all_posts, compiled.posts, views)

  RenderDatabase(
    single_posts: posts,
    pages: pages,
    index_pages: index_pages,
    tag_pages: tag_pages,
    year_pages: year_pages,
    month_pages: month_pages,
    feed: feed,
  )
}

pub fn render_posts(
  db: Database,
  post_contents: Dict(PostID, CompiledPost),
  views: Views,
) -> List(RenderedSinglePost) {
  let all_posts = database.get_posts_with_ids(db, ordered_tree.Desc)

  all_posts
  |> list.map(fn(post_with_id) {
    let assert Ok(content) = dict.get(post_contents, post_with_id.id)
    let rendered =
      views.base(
        views.single_post_full(content),
        views.meta(views.Post(post_with_id.post)),
        post_with_id.post.title,
      )
    RenderedSinglePost(post_with_id.post, rendered)
  })
}

pub fn render_pages(
  _db: Database,
  compiled_pages: List(CompiledPage),
  views: Views,
) {
  list.map(compiled_pages, fn(page) {
    let rendered =
      views.base(
        views.page(page),
        views.meta(views.Page(page.orig)),
        page.orig.title,
      )
    RenderedPage(page.orig, rendered)
  })
}

fn render_index_pages(
  config: Configuration,
  posts: List(database.PostWithID),
  compiled_posts: Dict(PostID, CompiledPost),
  views: Views,
) {
  pageify_posts(
    posts,
    config,
    compiled_posts,
    views,
    "",
    config.paths.index,
    element.none(),
  )
}

fn render_tag_pages(
  config: Configuration,
  db: Database,
  compiled_posts: Dict(PostID, CompiledPost),
  views: Views,
) {
  let tags = database.tags(db)

  dict.map_values(tags, fn(tag, posts) {
    let posts = ordered_tree.to_list(posts, ordered_tree.Desc)
    pageify_posts(
      posts,
      config,
      compiled_posts,
      views,
      tag,
      config.paths.tag(tag),
      element.none(),
    )
  })
}

fn render_year_pages(
  config: Configuration,
  db: Database,
  compiled_posts: Dict(PostID, CompiledPost),
  views: Views,
) {
  let years = database.years(db)

  dict.map_values(years, fn(year, posts) {
    let posts =
      date.months
      |> list.reverse()
      |> list.map(fn(month) {
        posts
        |> dict.get(month)
        |> result.map(ordered_tree.to_list(_, ordered_tree.Desc))
        |> result.unwrap([])
      })
      |> list.flatten()

    pageify_posts(
      posts,
      config,
      compiled_posts,
      views,
      string.replace(
        g_(config.l10n.context, "Archives for {year}"),
        "{year}",
        int.to_string(year),
      ),
      config.paths.year(year),
      element.none(),
    )
  })
}

fn render_month_pages(
  config: Configuration,
  db: Database,
  compiled_posts: Dict(PostID, CompiledPost),
  views: Views,
) {
  let years = database.years(db)

  dict.fold(years, dict.new(), fn(acc, year, year_posts) {
    dict.fold(year_posts, acc, fn(acc2, month, month_posts) {
      let posts = ordered_tree.to_list(month_posts, ordered_tree.Desc)
      dict.insert(
        acc2,
        #(year, month),
        pageify_posts(
          posts,
          config,
          compiled_posts,
          views,
          g_(config.l10n.context, "Archives for {month} {year}")
            |> string.replace(
            "{month}",
            date.month_to_string(month, config.l10n.context),
          )
            |> string.replace("{year}", int.to_string(year)),
          config.paths.month(year, month),
          element.none(),
        ),
      )
    })
  })
}

fn render_feed(
  posts: List(database.PostWithID),
  compiled_posts: Dict(PostID, CompiledPost),
  views: Views,
) {
  let posts =
    list.map(posts, fn(post) {
      let assert Ok(rendered) = dict.get(compiled_posts, post.id)
      rendered
    })
  views.feed(posts)
}

fn pageify_posts(
  posts: List(PostWithID),
  config: Configuration,
  compiled_posts: Dict(PostID, CompiledPost),
  views: Views,
  title_prefix: String,
  root_path: String,
  extra_header: Element(Nil),
) {
  let posts = case posts {
    [] -> [[]]
    posts -> list.sized_chunk(posts, config.rendering.posts_per_page)
  }
  let total_pages = list.length(posts)
  list.index_map(posts, fn(page_posts, index) {
    let page = index + 1

    let info =
      ListInfo(
        root_path: root_path,
        current_page: page,
        total_pages: total_pages,
        posts: list.map(page_posts, fn(post_with_id) {
          let assert Ok(post) = dict.get(compiled_posts, post_with_id.id)
          post
        }),
        extra_header: extra_header,
      )

    let page_content =
      views.base(
        views.list_page(info),
        views.meta(views.Other(title_prefix, "")),
        title_prefix,
      )
    ListPage(page: page, content: page_content)
  })
}
