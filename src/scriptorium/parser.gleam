//// Parsing reads the input files into the database.

import gleam/list
import gleam/regex
import gleam/result
import gleam/string
import kielet.{gettext as g_}
import scriptorium/config.{type Configuration, type ParseError, ParseError}
import scriptorium/models/database.{type Database}
import scriptorium/parser/common
import scriptorium/parser/menu
import scriptorium/parser/page
import scriptorium/parser/post
import simplifile

/// The default path where input files are read from.
const default_data_path = "./data"

/// The default parser.
pub fn default_parse(config: Configuration) -> Result(Database, ParseError) {
  parse_posts(database.new(), post_path())
  |> result.try(parse_pages(_, page_path()))
  |> result.try(parse_menu(_, menu_path(), config))
}

/// Parse posts from the given path into the database.
pub fn parse_posts(db: Database, path: String) -> Result(Database, ParseError) {
  use filenames <- result.try(
    simplifile.read_directory(path)
    |> result.map_error(fn(err) {
      ParseError(path <> ": " <> string.inspect(err))
    }),
  )

  let assert Ok(filename_regex) =
    regex.compile(
      post.filename_regex,
      regex.Options(case_insensitive: False, multi_line: False),
    )

  let filenames =
    list.filter(filenames, fn(file) {
      string.ends_with(file, common.filename_postfix)
      && regex.check(filename_regex, file)
    })

  use posts <- result.try(
    result.all(
      list.map(filenames, fn(file) {
        use contents <- result.try(
          simplifile.read(path <> "/" <> file)
          |> result.map_error(fn(err) {
            ParseError(file <> ": " <> string.inspect(err))
          }),
        )

        post.parse(file, contents)
        |> result.map_error(fn(err) {
          ParseError(file <> ": " <> string.inspect(err))
        })
      }),
    ),
  )

  Ok(list.fold(posts, db, database.add_post))
}

/// Parse pages from the given path into the database.
pub fn parse_pages(db: Database, path: String) -> Result(Database, ParseError) {
  use filenames <- result.try(
    simplifile.read_directory(path)
    |> result.map_error(fn(err) {
      ParseError(path <> ": " <> string.inspect(err))
    }),
  )

  use pages <- result.try(result.all(
    filenames
    |> list.filter(fn(filename) {
      string.ends_with(filename, common.filename_postfix)
    })
    |> list.map(fn(file) {
      use contents <- result.try(
        simplifile.read(path <> "/" <> file)
        |> result.map_error(fn(err) {
          ParseError(file <> ": " <> string.inspect(err))
        }),
      )

      page.parse(file, contents)
      |> result.map_error(fn(err) {
        ParseError(file <> ": " <> string.inspect(err))
      })
    }),
  ))

  Ok(list.fold(pages, db, database.add_page))
}

/// Parse the menu from the given file into the database.
pub fn parse_menu(
  db: Database,
  file: String,
  config: Configuration,
) -> Result(Database, ParseError) {
  case simplifile.verify_is_file(file) {
    Ok(True) -> {
      use contents <- result.try(
        simplifile.read(file)
        |> result.map_error(fn(err) {
          ParseError(file <> ": " <> string.inspect(err))
        }),
      )

      use menu <- result.try(
        menu.parse(contents)
        |> result.map_error(fn(err) {
          ParseError(string.replace(
            g_(config.l10n.context, "Menu parsing failed: {err}"),
            "{err}",
            string.inspect(err),
          ))
        }),
      )

      Ok(database.set_menu(db, menu))
    }

    _ -> Ok(db)
  }
}

fn post_path() {
  default_data_path <> "/posts"
}

fn page_path() {
  default_data_path <> "/pages"
}

fn menu_path() {
  default_data_path <> "/menu"
}
