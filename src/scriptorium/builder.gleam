//// The builder contains convenience functions to interface with the different
//// parts of scriptorium, using the configuration to control what is done.

import gleam/result
import scriptorium/compiler.{type CompileDatabase}
import scriptorium/config.{type Configuration}
import scriptorium/models/database.{type Database}
import scriptorium/rendering/database as render_database

/// Something failed when building the blog.
pub type BuildError {
  ParseError(err: config.ParseError)
  WriteError(err: config.WriteError)
}

/// Parse the blog's input files.
pub fn parse(config: Configuration) {
  config.parser(config)
  |> result.map_error(ParseError)
}

/// Compile the post and page content into HTML strings.
pub fn compile(db: Database, config: Configuration) {
  config.compiling.database_compiler(db, config.compiling.item_compiler)
}

/// Render the content into HTML pages.
pub fn render(db: Database, compiled: CompileDatabase, config: Configuration) {
  config.rendering.renderer(db, compiled, config)
}

/// Write the render database into files.
pub fn write(posts: render_database.RenderDatabase, config: Configuration) {
  config.writer(posts, config)
  |> result.map_error(WriteError)
}
