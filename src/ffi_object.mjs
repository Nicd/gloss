export function create() {
  return {};
}

export function set(obj, prop, val) {
  return { ...obj, [prop]: val };
}

export function get(obj, prop) {
  return obj[prop];
}
