# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR Mikko Ahlroth
# This file is distributed under the same license as the Scriptorium package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Scriptorium 2.0.0\n"
"Report-Msgid-Bugs-To: mikko@ahlroth.fi\n"
"POT-Creation-Date: 2024-05-30 21:47+0300\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/scriptorium/parser.gleam:119
msgid "Menu parsing failed: {err}"
msgstr ""

#: src/scriptorium/renderer.gleam:177
msgid "Archives for {year}"
msgstr ""

#: src/scriptorium/renderer.gleam:206
msgid "Archives for {month} {year}"
msgstr ""

#: src/scriptorium/rendering/views/base.gleam:95
#: src/scriptorium/rendering/views/single_post.gleam:47
msgid "Tags"
msgstr ""

#: src/scriptorium/rendering/views/base.gleam:103
msgid "Archives"
msgstr ""

#: src/scriptorium/rendering/views/list_page.gleam:22
msgid "Pages"
msgstr ""

#: src/scriptorium/rendering/views/nav.gleam:20
msgid "Previous"
msgstr ""

#: src/scriptorium/rendering/views/nav.gleam:38
msgid "current page"
msgstr ""

#: src/scriptorium/rendering/views/nav.gleam:64
msgid "Next"
msgstr ""

#: src/scriptorium/rendering/views/single_post.gleam:70
msgid "Read more…"
msgstr ""

#: src/scriptorium/rendering/views/single_post.gleam:88
msgid "{date}, {time}"
msgstr ""

#: src/scriptorium/rendering/views/single_post.gleam:102
msgid "Posted on {datetime}."
msgstr ""

#: src/scriptorium/utils/date.gleam:126
msgid "January"
msgstr ""

#: src/scriptorium/utils/date.gleam:127
msgid "February"
msgstr ""

#: src/scriptorium/utils/date.gleam:128
msgid "March"
msgstr ""

#: src/scriptorium/utils/date.gleam:129
msgid "April"
msgstr ""

#: src/scriptorium/utils/date.gleam:130 src/scriptorium/utils/date.gleam:148
msgid "May"
msgstr ""

#: src/scriptorium/utils/date.gleam:131
msgid "June"
msgstr ""

#: src/scriptorium/utils/date.gleam:132
msgid "July"
msgstr ""

#: src/scriptorium/utils/date.gleam:133
msgid "August"
msgstr ""

#: src/scriptorium/utils/date.gleam:134
msgid "September"
msgstr ""

#: src/scriptorium/utils/date.gleam:135
msgid "October"
msgstr ""

#: src/scriptorium/utils/date.gleam:136
msgid "November"
msgstr ""

#: src/scriptorium/utils/date.gleam:137
msgid "December"
msgstr ""

#: src/scriptorium/utils/date.gleam:144
msgid "Jan"
msgstr ""

#: src/scriptorium/utils/date.gleam:145
msgid "Feb"
msgstr ""

#: src/scriptorium/utils/date.gleam:146
msgid "Mar"
msgstr ""

#: src/scriptorium/utils/date.gleam:147
msgid "Apr"
msgstr ""

#: src/scriptorium/utils/date.gleam:149
msgid "Jun"
msgstr ""

#: src/scriptorium/utils/date.gleam:150
msgid "Jul"
msgstr ""

#: src/scriptorium/utils/date.gleam:151
msgid "Aug"
msgstr ""

#: src/scriptorium/utils/date.gleam:152
msgid "Sep"
msgstr ""

#: src/scriptorium/utils/date.gleam:153
msgid "Oct"
msgstr ""

#: src/scriptorium/utils/date.gleam:154
msgid "Nov"
msgstr ""

#: src/scriptorium/utils/date.gleam:155
msgid "Dec"
msgstr ""

#: src/scriptorium/utils/date.gleam:161
msgid "{day} {month_short_str} {year}"
msgstr ""

#: src/scriptorium/utils/time.gleam:44
msgid "{h24_0}:{min_0}"
msgstr ""

#: src/scriptorium/utils/time.gleam:77
msgid "<00> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:78
msgid "<01> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:79
msgid "<02> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:80
msgid "<03> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:81
msgid "<04> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:82
msgid "<05> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:83
msgid "<06> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:84
msgid "<07> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:85
msgid "<08> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:86
msgid "<09> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:87
msgid "<10> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:88
msgid "<11> am"
msgstr ""

#: src/scriptorium/utils/time.gleam:89
msgid "<12> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:90
msgid "<13> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:91
msgid "<14> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:92
msgid "<15> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:93
msgid "<16> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:94
msgid "<17> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:95
msgid "<18> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:96
msgid "<19> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:97
msgid "<20> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:98
msgid "<21> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:99
msgid "<22> pm"
msgstr ""

#: src/scriptorium/utils/time.gleam:100
msgid "<23> pm"
msgstr ""
