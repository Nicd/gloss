��    &      L  5   |      P     Q     U     [     d     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                	                    $     :     C     P  	   T     ^     c     p          �    �     �     �     �     �     �               !     '     0     6     ?     E     N  	   T     ^     d     m  	   t     ~  *   �     �     �  	   �     �     �     �     �  	   �     �                    !     -     ?     W                               
         %      &   $                                            #       "       !                                     	                                  Apr April Archives Archives for {month} {year} Archives for {year} Aug August Dec December Feb February Jan January Jul July Jun June Mar March May Menu parsing failed: {err} Next Nov November Oct October Pages Posted on {datetime}. Previous Read more… Sep September Tags current page {date}, {time} {day} {month_short_str} {year} {h24_0}:{min_0} Project-Id-Version: Scriptorium 2.0.0
Report-Msgid-Bugs-To: mikko@ahlroth.fi
PO-Revision-Date: 2024-05-30 21:48+0300
Last-Translator: 
Language-Team: 
Language: fi
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 3.4.4
 huhti huhtikuu Arkistot Arkistot {month}lle {year} Arkistot vuodelle {year} heinä elokuu joulu joulukuu helmi helmikuu tammi tammikuu kesä heinäkuu touko kesäkuu maalis maaliskuu toukokuu Valikon jäsentäminen epäonnistui: {err} Seuraava marras marraskuu loka lokakuu Sivut Julkaistu {datetime}. Edellinen Lue lisää… syys syyskuu Tagit tämä sivu {date} klo {time} {day}.{month_no}.{year} {h24_0}:{min_0} 